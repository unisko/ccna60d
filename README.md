## 60天通过CCNA

__Cisco CCNA in 60 Days__


___

- Gitbook：[ccna60d.xfoss.com](https://ccna60d.xfoss.com/)

- PDF 版本：[ccna60d.pdf](./ccna60d.pdf)

- [Android APP](https://github.com/gnu4cn/ccna60d-app/releases/download/v0.1.3/ccna60d.xfoss.com_v0.1.3.apk)


你可以在 https://github.com/gnu4cn/ccna60d 上 fork 本项目，并提交你的修正。


本书结合了学习技巧，包括阅读、复习、背书、测试以及 hands-on 实验。

> 本书译者用其业余时间完成本书的翻译工作，并将其公布到网上，以方便你对网络技术的学习掌握，为使译者更有动力改进翻译及完成剩下章节，你可以捐赠译者:  

| <img src="images/alipay-banner.png" height="15" /> | <img src="images/wechat-pay-banner.png" height="15" /> | <img src="images/logotop.png" height="15" /> |
| :----: | :----: | :----: |
| <img src="images/633086908.jpg" /> | <img src="images/611739062.jpg" /> | <img src="images/btc-qrcode.png" /> |


## 捐赠记录

*2017-08-03*

- “十円”通过支付宝进行了捐赠，并留言“谢谢译者的辛勤付出！”

*2017-05-21*

- “远”通过支付宝进行了捐赠，并留言“60天通过ccna对我帮助很大期待更新”

## 更新记录

*2020-10-27*

- 生成PDF版本

*2019-10-30*

- 完成全部章节翻译
- 重新通过 Gitbook 进行发布

*2017-07-16*

- 完成第37天--EIGRP故障排除章节
- 完成第38天--IPv6下的EIGRP章节

*2017-07-14*

- 完成第36天--EIGRP 章节的修订，EIGRP已无问题
